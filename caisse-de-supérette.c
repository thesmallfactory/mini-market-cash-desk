#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

FILE *f;

int compteur = 0;
int i, n;

double prixunitaire;
int fonction;
int indice;

int j, m;
char code[10];
double nombre;

double total;
int h;
char ligne[256];


double somme;

void charger_prix(double prix[][2]){
	f = fopen("Prix.txt", "r");
	if (f == NULL){
		printf("\n    Il y a une erreur de chargement du fichier \x22Prix\x22 ; le programme doit s'ar-\n r\x88ter...\n\n");
		exit(2);
	}
	compteur++;
	i = 0;
	do{
		fscanf(f, "%lf", &prix[i][0]);
		fscanf(f, "%lf", &prix[i][1]);
		i++;
	}
	while (fgets(ligne, 255, f) != NULL);
	n = i;
	fclose(f);
}

double chercher_prix(double prix[][2], double codebarre){
	prixunitaire = 0;
	charger_prix(prix);
	if (fonction == 2){
	
		LigneF2I:
		printf("\n\n    Entrez le code-barre de l'article consid\x82r\x82 (ou \x22");
		printf("0\x22, si vous voulez annuler\n la fonction en cours) : ");
		scanf("%lf", &codebarre);
		if (getchar() != '\n'){
			fflush(stdin);
			goto LigneF2I;
		}
		if (codebarre == 0){
			printf("\n\n    La fonction \x22");
			printf("Chercher le prix unitaire d'un article\x22 est annul\x82");
			printf("e...\n");
			goto FinF2;
		}
	}
	int borne;
	int inf, sup;
	indice = 0;
	for (borne = 1; borne <= 26; borne++){
		inf = 70000000 + borne * 100000 +  1000;
		sup = 70000000 + borne * 100000 + 26000;
		if (codebarre > inf && codebarre < sup){
			indice = 1;
			i = 0;
			while (codebarre != prix[i][0] && i < n){
				i++;
			}
			if (i == n){

				LigneF2II:
				if (fonction == 2){
					printf("\n\n    L'article dont le code-barre est %.0lf n'a encore jamais \x82t\x82 enregistr\x82.\n Veuillez saisir \x28", codebarre);
					printf("en Eu\x29 le prix de ce dernier ; sinon, entrez \x22");
					printf("0\x22 si vous vou-\n lez annuler la fonction en cours : ");
				}
				else{
					printf("\n\n    L'article dont le code-barre est %.0lf n'a encore jamais \x82t\x82 enregistr\x82.\n Veuillez saisir \x28", codebarre);
					printf("en Eu\x29 le prix de ce dernier ; sinon, entrez \x22");
					printf("0\x22 si vous vou-\n lez annuler la saisie du dernier article : ");
				}
				scanf("%lf", &prixunitaire);
				if (getchar() != '\n'){
					fflush(stdin);
					goto LigneF2II;
				}
				if (prixunitaire == 0){
					if (fonction == 2){
						printf("\n\n    La fonction \x22");
						printf("Chercher le prix unitaire d'un article\x22 est annul\x82");
						printf("e...\n");
					}
					else{
						printf("\n\n    La saisie du dernier article est annul\x82");
						printf("e...\n");
					}
				}
			}
			else{
				prixunitaire = prix[i][1];
			}
			goto FinF2;
		}
	}
	printf("\n\n    Soit A l'article consid\x82r\x82 \x28");
	printf("A peut \x88tre du jambon \x85 l'\x82touff\x82");
	printf("e\x29 ; soit C la\n cat\x82gorie de A \x28si A \x82tait du jambon \x28\x85 l'\x82touff\x82");
	printf("e\x29, alors C serait la charcu-\n terie\x29.\n    Tout code-barre doit commencer par un \x22");
	printf("7\x22, continuer par deux chiffres ser-\n vant \x85 d\x82signer le rang \x28");
	printf("alphab\x82tique\x29 de l'initiale du nom de C \x28s'il s'agis-\n sait de la charcuterie, alors il serait question de 0 et 3\x29 puis, encore, deux\n chiffres servant, \x82galement, \x85 d\x82signer le rang \x28");
	printf("aussi alphab\x82tique\x29 de la se-\n conde lettre du nom de C \x28");
	printf("il serait \x82videmment question de 0 et 8 s'il s'agis-\n sait de la charcuterie\x29, et finir par un num\x82ro \x85 trois chiffres correspondant\n au rang, \x85 l'int\x82rieur de C, de A. \x28");
	printf("Ainsi, le code-barre du jambon \x28\x85 l'\x82touf-\n f\x82");
	printf("e\x29 est, par exemple, 70308002...\x29\n");
	if (fonction == 2){
		goto LigneF2I;
	}

	FinF2:
	return prixunitaire;
}

void saisir_facture(double prix[][2], double fact[][3]){
	j = 0;
	printf("\n\n    Pour indiquer au programme que la facture est termin\x82");
	printf("e vous entrerez la va-\n leur qui suit : \x22");
	printf("0000000\x22...\n");

	LigneF3I:
	if (j == 0){
		printf("\n\n    Entrez le code-barre du 1er article consid\x82r\x82 (ou \x22");
		printf("0\x22, si vous voulez annu-\n ler la fonction en cours) : ");
	}
	else{
		printf("\n\n    Entrez le code-barre du %d\x8Ame article consid\x82r\x82 : ", j + 1);
	}
	scanf("%s", &code);
	if (strcmp(code, "0") == 0){
		printf("\n\n    La fonction \x22Saisir une facture\x22 est annul\x82");
		printf("e...\n");
		j = 0;
		goto FinF3;
	}
	if (strcmp(code, "0000000") == 0){
		if (j == 0){
			printf("\n\n    Aucun article n'a \x82t\x82 enregistr\x82 ; la fonction \x22Saisir une facture\x22 est par\n cons\x82quent annul\x82");
			printf("e...\n");
		}
		else{
			printf("\n\n    La saisie est termin\x82");
			printf("e...\n");
		}
		goto FinF3;
	}
	double codebarre = 0;
	sscanf(code, "%lf", &codebarre);
	if (pow(10, strlen(code) - 1) > codebarre){
		printf("\n\n    L'expression saisie n'est pas bien form\x82");
		printf("e ; veuillez recommencer !\n");
		goto LigneF3I;
	}
	chercher_prix(prix, codebarre);
	if (indice == 0){
		printf("\n\n    Veuillez recommencer !\n");
		goto LigneF3I;
	}
	else{
		indice = 0;
		if (prixunitaire == 0){
			
			j = j - 1;
			goto FinF3;
		}
	}

	LigneF3II:
	if (j == 0){
		printf("\n\n    Entrez la quantit\x82 du 1er article consid\x82r\x82 (ou \x22");
		printf("0\x22, si vous voulez annuler\n la fonction en cours) : ");
	}
	else{
		printf("\n\n    Entrez la quantit\x82 du %d\x8Ame article consid\x82r\x82 : ", j + 1);
	}
	scanf("%lf", &nombre);
	if (getchar() != '\n'){
		fflush(stdin);
		goto LigneF3II;
	}
	if (nombre == 0){
		printf("\n\n    La saisie du dernier article est annul\x82");
		printf("e...\n");
		exit;
	}
	else{
		fact[j][0] = codebarre;
		fact[j][1] = nombre;
		fact[j][2] = prixunitaire;
		j++;
		goto LigneF3I;
	}
	FinF3:
	m = j;
}

void affich_facture(double fact[][3]){
	total = 0;
	h = 0;
	for (j = 0; j < m; j++){
		f = fopen("Produits.txt", "r");
		if (f == NULL){
			printf("Erreur de lecture du fichier Produits.\n");
			exit(2);
		}
		do{
			double codebarre = 0;
			fscanf(f, "%lf", &codebarre);
			if (codebarre == fact[j][0]){
				printf("\n %.0lf : ", fact[j][0]);
				fgets(ligne, 255, f);
				printf("%s", ligne);
				printf("      x %.0lf =  %7.2lf Eu\n", fact[j][1], fact[j][1] * fact[j][2]);
				h++;
				break;
			}
		}
		while (fgets(ligne, 255, f) != NULL);
		if (h == j){
			printf("\n %.0lf : \x22Produit g\x82n\x82ral\x22\n", fact[j][0]);
			printf("      x %.0lf =  %7.2lf Eu\n", fact[j][1], fact[j][1] * fact[j][2]); h++;
		}
		total = total + fact[j][1] * fact[j][2];
		fclose(f);
	}
}

void enregistre_facture(double fact[][3]){
	char RE;

	LigneF5:
	printf("\n\n    D'accord pour paiement ? (Entrez \x22O\x22 \x28voire \x22o\x22\x29 pour \x22Oui !\x22, ou \x22N\x22 \x28voi-\n re \x22n\x22\x29 pour \x22Non !\x22 : ");
	scanf("%c", &RE);
	if (getchar() != '\n'){
		fflush(stdin);
		goto LigneF5;
	}
	if (RE == 'O' || RE == 'o'){
		int NF = 0;
		f = fopen("Facture.txt", "r");
		if (f != NULL){
			NF = 1;
			int C;
			while((C = fgetc(f)) != EOF){
				if(C == '\n'){
					NF++;
				}
			}
		}
		fclose(f);
		f = fopen("Facture.txt", "a");
		if (NF > 0){
			fprintf(f, "\n");
		}
		fprintf(f, "facture %03d ", NF + 1);
		for (j = 0; j < m; j++){
			fprintf(f, "%.0lf %02.0lf %.2lf ", fact[j][0], fact[j][1], fact[j][2]);
		}
		fprintf(f, "0000000");
		fclose(f);
		printf("\n\n    La facture %03d est enregistr\x82", NF + 1);
		printf("e...\n");
		exit;
	}
	else{
		if (RE == 'N' || RE == 'n'){
			j = 0;
			exit;
		}
		else{
			goto LigneF5;
		}
	}
}


double toutesfactures(double fact[][3]){
	somme = 0;
	char information[10];
	i = 0;
	f = fopen("Facture.txt", "r");
	do{
		do{
			fscanf(f, "%s", &information);
		}
		while (strcmp(information, "facture") != 0);
		fscanf(f, "%s", &information);
		fscanf(f, "%s", &information);
		while (strcmp(information, "0000000") != 0){
			for (h = 0; h < 3; h++){
				sscanf(information, "%lf", &fact[i][h]);
				fscanf(f, "%s", &information);
			}
			i++;
		}
	}
	while (fgets(ligne, 255, f) != NULL);
	fclose(f);
	n = i;
	m = n;
	for (i = 0; i < n; i++){
		for (h = 1; h < m; h++){
			if (fact[h][0] == fact[i][0] && h != i){
				fact[i][1] = fact[i][1] + fact[h][1];
				for (h; h < m; h++){
					fact[h][0] = fact[h + 1][0];
					fact[h][1] = fact[h + 1][1];
					fact[h][2] = fact[h + 1][2];
				}
				m--;
				i--;
			}
		}
	}
	for (i = 0; i < m; i++){
		somme = somme + fact[i][1] * fact[i][2];
	}
	return somme;
}

void ordprod(double fact[][3]){
	toutesfactures(fact);
	double vente[m];
	double provisoire;
	double temporaire[3];
	n = m;
	for (i = 0; i < n; i++){
		vente[i] = fact[i][1] * fact[i][2];
	}
	for (h = 0; h < m; h++){
		for (i = 0; i < n; i++){
			if (vente[i] < vente[i + 1]){
				provisoire = vente[i + 1];
				temporaire[0] = fact[i + 1][0];
				temporaire[1] = fact[i + 1][1];
				temporaire[2] = fact[i + 1][2];
				vente[i + 1] = vente[i];
				fact[i + 1][0] = fact[i][0];
				fact[i + 1][1] = fact[i][1];
				fact[i + 1][2] = fact[i][2];
				vente[i] = provisoire;
				fact[i][0] = temporaire[0];
				fact[i][1] = temporaire[1];
				fact[i][2] = temporaire[2];
			}
		}
	}
}

double maxprod(double fact[][3]){
	ordprod(fact);
	printf("\n\n    La plus grande somme totale des ventes est celle de l'article dont le code-\n barre est %.0lf.\n", fact[0][0]);
	return;
}



int main(){
	double prix[1000][2];
	double codebarre;
	double fact[1000][3];

	Ligne1:
	printf("\n\n   FONCTIONS DE BASE\n");
	printf("\n \xDA\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC2\xC4\xC4\xC4\xBF\n");
	printf(" \xB3 Charger le prix unitaire de chaque article en fonction du code-barre.. \xB3 1 \xB3\n");
	printf(" \xB3 Chercher le prix unitaire d'un article................................ \xB3 2 \xB3\n");
	printf(" \xB3 Saisir une facture.................................................... \xB3 3 \xB3\n");
	printf(" \xB3 Afficher la facture................................................... \xB3 4 \xB3\n");
	printf(" \xB3 Enregistrer la facture................................................ \xB3 5 \xB3\n");
	printf(" \xB3 Afficher les fonctions de haut niveau................................. \xB3 6 \xB3\n");
	printf(" \xB3 Arr\x88ter le programme.................................................. \xB3 0 \xB3\n");
	printf(" \xC0\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC1\xC4\xC4\xC4\xD9\n");
	switch (compteur){
		case 0: printf("\n    S\x82lectionnez une fonction de base : ");
				break;
		default: printf("\n    S\x82lectionnez \x85 nouveau une fonction de base : ");
	}
	scanf("%d", &fonction);
	if (getchar() != '\n'){
		fflush(stdin);
		goto Ligne1;
	}
	switch (fonction){
		case 1: charger_prix(prix);
				printf("\n\n    Le programme charge le fichier \x22Prix\x22...\n");
				int RC;
				
				Ligne11:
				printf("\n\n    Si vous souhaitez que le programme affiche le prix unitaire de chaque arti-\n cle en fonction du code-barre, entrez \x22");
				printf("1\x22 ; sinon, si vous voulez s\x82lectionner\n une autre fonction, entrez \x22");
				printf("0\x22 : ");
				scanf("%d", &RC);
				if (getchar() != '\n'){
					fflush(stdin);
					goto Ligne11;
				}
				switch (RC){
					case 1: printf("\n    \xDA\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC2\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xBF\n");
							for (i = 0; i < n; i++){
								printf("    \xB3 %.0lf \xB3", prix[i][0]);
								printf(" %7.2lf \xB3 \n", prix[i][1]);
							}
							printf("    \xC0\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC1\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xD9\n");
							break;
					case 0: break;
					default: goto Ligne11;
				}
				goto Ligne1;
				break;
		case 2: printf("\n\n    Quel est l'article dont vous d\x82sirez conna\x8Ctre le prix ?\n");
				chercher_prix(prix, codebarre);
				if (prixunitaire != 0){
					printf("\n\n    Le prix unitaire de l'article en question est %.2lf Eu.\n", prixunitaire);
				};
				goto Ligne1;
				break;
		case 3: saisir_facture(prix, fact);
				goto Ligne1;
				break;
		case 4: if (j == 0){
					printf("\n\n    Il faut saisir une facture !\n");
					goto Ligne1;
				}
				else{
					printf("\n");
					affich_facture(fact);
					printf("\n ------------------------------------------------------------------------------\n Total de la facture :: %.2lf Eu.\n ------------------------------------------------------------------------------", total);
					goto Ligne1;
				};
				break;
		case 5: if (j == 0){
					printf("\n\n    Il faut saisir une facture !\n");
					goto Ligne1;
				}
				else{
					enregistre_facture(fact);
					j = 0;
					goto Ligne1;
				};
				break;
		case 6: f = fopen("Facture.txt", "r");
				if (f == NULL){
					printf("\n\n    Aucune vente n'a \x82t\x82 enregistr\x82");
					printf("e...\n");
					goto Ligne1;
				}
				fclose(f);
				char FH;
				
				Ligne12:
				printf("\n\n   FONCTIONS DE HAUT NIVEAU\n");
				printf("\n\xDA\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC2\xC4\xC4\xC4\xBF");
				printf("\xB3 Agr\x82ger toutes les factures en une seule................................ \xB3 A \xB3");
				printf("\xB3 Trier les sommes totales des ventes dans l'ordre d\x82");
				printf("croissant............ \xB3 B \xB3");
				printf("\xB3 Donner le code-barre de l'article dont les ventes ont rapport\x82 le plus.. \xB3 C \xB3");
				printf("\xB3 Retourner aux fonctions de base......................................... \xB3 D \xB3");
				printf("\xB3 Arr\x88ter le programme.................................................... \xB3 N \xB3");
				printf("\xC0\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC1\xC4\xC4\xC4\xD9");
				printf("\n    S\x82lectionnez une fonction de haut niveau : ");
				scanf("%c", &FH);
				if (getchar() != '\n'){
					fflush(stdin);
					goto Ligne12;
				}
				switch (FH){
					case 'A': toutesfactures(fact);
							  int RA;
				
							  Ligne12A:
							  printf("\n\n    Si vous souhaitez que le programme affiche le r\x82sultat de l'agr\x82gation, en-\n trez \x22");
							  printf("1\x22 ; sinon, entrez \x22");
							  printf("0\x22 : ");
							  scanf("%d", &RA);
							  if (getchar() != '\n'){
								  fflush(stdin);
								  goto Ligne12A;
							  }
							  printf("%d\n", m);
							  switch (RA){
								  case 1: printf("\n    \xDA\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC2\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xBF\n");
										  for (h = 0; h < m; h++){
											  printf("    \xB3 %.0lf \xB3", fact[h][0]);
											  printf(" %7.2lf \xB3 \n", fact[h][1] * fact[h][2]);
										  }
										  printf("    \xC0\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC1\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xD9\n");
										  break;
								  case 0: break;
								  default: goto Ligne12A;
							  }
							  printf("\n\n ------------------------------------------------------------------------------\n Somme totale des ventes dans toutes les factures :: %.2lf Eu.\n ------------------------------------------------------------------------------\n", somme);
							  goto Ligne12;
							  break;
					case 'B': ordprod(fact);
							  printf("\n\n    Le programme trie le fichier \x22");
							  printf("Facture\x22...\n");
							  int RT;
				
							  Ligne12B:
							  printf("\n\n    Si vous souhaitez que le programme affiche le r\x82sultat du tri, entrez \x22");
							  printf("1\x22 ;\n sinon, entrez \x22");
							  printf("0\x22, si vous voulez s\x82lectionner une autre fonction \x28");
							  printf("de haut ni-\n veau\x29 : ");
							  scanf("%d", &RT);
							  if (getchar() != '\n'){
								  fflush(stdin);
								  goto Ligne12B;
							  }
							  switch (RT){
								  case 1: printf("\n    \xDA\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC2\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xBF\n");
										  for (i = 0; i < n; i++){
											  printf("    \xB3 %7.2lf \xB3", fact[i][1] * fact[i][2]);
											  printf(" %.0lf \xB3\n", fact[i][0]);
										  }
										  printf("    \xC0\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC1\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xC4\xD9\n");
										  break;
								  case 0: break;
								  default: goto Ligne12B;
							  }
							  goto Ligne12;
							  break;
					case 'C': maxprod(fact);
							  goto Ligne12;
							  break;
					case 'D': goto Ligne1;
							  break;
					case 'N': printf("\n\n    Arr\x88t du programme ; merci et \x85 bient\x93t !\n\n");
							  exit;
							  break;
					default: goto Ligne12;
					
				}
				break;
		case 0: printf("\n\n    Arr\x88t du programme ; merci et \x85 bient\x93t !\n\n");
				exit;
				break;
		default: goto Ligne1;
	}
	return 0;
}
